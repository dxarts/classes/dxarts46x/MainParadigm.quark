MainParadigm : Read Me
========================
_MainParadigm: A lightweight project management system._

&nbsp;

&nbsp;

Installing
==========

Distributed via [MainParadigm quark](https://gitlab.com/dxarts/classes/dxarts46x/MainParadigm.quark).

Start by reviewing the Quark installation instructions
[found here](https://github.com/supercollider-quarks/quarks#installing). See
also [Using Quarks](http://doc.sccode.org/Guides/UsingQuarks.html).

With [git](https://git-scm.com/) installed, you can easily install the
[MainParadigm quark](https://gitlab.com/dxarts/classes/dxarts46x/MainParadigm.quark)
directly by running the following line of code in SuperCollider:

    Quarks.install("https://gitlab.com/dxarts/classes/dxarts46x/MainParadigm.quark");



Feedback and Bug Reports
========================

Known issues are logged at
[GitLab](https://gitlab.com/dxarts/classes/dxarts46x/MainParadigm.quark/-/issues).

&nbsp;

&nbsp;



List of Changes
---------------

Version 1.0.0

* Initial release.

Version pre-release

* Refactor MainParadigm as quark separate from dxarts461.quark.


&nbsp;

&nbsp;

Authors
=======

&nbsp;

Copyright James Wenlock and the DXARTS Community, 2019-2020.

Department of Digital Arts and Experimental Media (DXARTS)  
University of Washington

&nbsp;


Contributors
------------

Version pre-release
*  Joseph Anderson (@joslloand)
*  James Wenlock (@wenloj)


Contribute
----------

As part of the wider SuperCollider community codebase, contributors are encouraged to observe the published SuperCollider guidelines found [here](https://github.com/supercollider/supercollider#contribute).


License
=======

The [MainParadigm quark](https://gitlab.com/dxarts/classes/dxarts46x/MainParadigm.quark) is free software available under [Version 3 of the GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). See [LICENSE](LICENSE) for details.
